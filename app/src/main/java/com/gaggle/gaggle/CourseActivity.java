package com.gaggle.gaggle;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.gaggle.gaggle.adapter.ListViewAdapter;
import com.gaggle.gaggle.model.Course;
import com.gaggle.gaggle.model.Post;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class CourseActivity extends AppCompatActivity {

    private ProgressBar progressBar;
    private FirebaseFirestore db;
    private List<Post> postList;
    private Course course;
    private ListViewAdapter listViewAdapter;

    public CourseActivity() {}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course);
        course = (Course) getIntent().getSerializableExtra("course");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(course.getCourseCode());
        setSupportActionBar(toolbar);

        // Set up create post button
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setupDialog().show();
            }
        });

        progressBar = (ProgressBar) this.findViewById(R.id.progress_bar);

        // add back arrow to toolbar
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        postList = new ArrayList<Post>();

        query();
    }

    private AlertDialog.Builder setupDialog() {
        // Set up create post dialog
        AlertDialog.Builder createPostDialog = new AlertDialog.Builder(this);

        final EditText edittext = new EditText(this);
        edittext.setHint("Type post...");


        FrameLayout container = new FrameLayout(this);
        FrameLayout.LayoutParams params = new  FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.leftMargin = getResources().getDimensionPixelSize(R.dimen.text_margin);
        params.rightMargin = getResources().getDimensionPixelSize(R.dimen.text_margin);
        edittext.setLayoutParams(params);
        container.addView(edittext);

        createPostDialog.setTitle("Make Post");

        createPostDialog.setView(container);

        createPostDialog.setPositiveButton("Post", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //What ever you want to do with the value
                String postMessage = edittext.getText().toString();
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                if(!postMessage.equals("")) {
                    Map<String, Object> post = new HashMap<>();
                    post.put("message", postMessage);
                    post.put("user", user.getDisplayName());
                    post.put("timestamp", String.valueOf(Instant.now().toEpochMilli()));
                    post.put("courseCode", course.getCourseCode());
                    post.put("commentCount", 0);
                    db.collection("posts").add(post);
                    query();

                    listViewAdapter.notifyDataSetChanged();
                }
                dialog.dismiss();
            }
        });

        createPostDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        });

        return createPostDialog;
    }

    private void query() {
        //Initialize Firestore db
        db = FirebaseFirestore.getInstance();

        db.collection("posts")
                .orderBy("timestamp", Query.Direction.DESCENDING)
                .whereEqualTo("courseCode", course.getCourseCode())
                .get()
                .addOnCompleteListener(getOnCompleteListener());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }

    public OnCompleteListener<QuerySnapshot> getOnCompleteListener() {
        return new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {

                    postList.clear();

                    for (QueryDocumentSnapshot document : task.getResult()) {

                        Map<String, Object> postMap = document.getData();
                        Post post = new Post(document.getId(),
                                (String) postMap.get("user"),
                                (String) postMap.get("timestamp"),
                                (String) postMap.get("message"),
                                (Long)(postMap.get("commentCount")));
                        postList.add(post);
                    }

                    ListView listView = findViewById(R.id.listView);
                    listViewAdapter = new ListViewAdapter(CourseActivity.this, postList);
                    listView.setAdapter(listViewAdapter);

                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        public void onItemClick(AdapterView<?> parent, View view,
                                                int position, long id) {
                            Intent intent = new Intent(CourseActivity.this, PostActivity.class);
                            intent.putExtra("post", postList.get(position));
                            startActivity(intent);
                        }
                    });
                } else {
                    System.out.println("Error getting documents." + task.getException());
                }

                progressBar.setVisibility(View.GONE);
            }
        };
    }
}
