package com.gaggle.gaggle.fragment;


import android.content.Intent;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import android.widget.AdapterView;
import android.widget.SearchView;
import android.widget.ArrayAdapter;

import com.gaggle.gaggle.CourseDetailActivity;
import com.gaggle.gaggle.R;
import com.gaggle.gaggle.model.Course;

public class CoursesFragment extends ListFragment  {

    private ArrayList<Course> courseList = new ArrayList<>();

    private SearchView editsearch;

    ArrayAdapter adapter;


    public static CoursesFragment newInstance() {
        CoursesFragment fragment = new CoursesFragment();
        return fragment;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.Courses, android.R.layout.simple_list_item_1);
        //setListAdapter(adapter);
        getListView().setAdapter(adapter);

        editsearch = (SearchView) getView().findViewById(R.id.search);
        editsearch.setQueryHint("Search Courses...");
        editsearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                String text = newText;
                adapter.getFilter().filter(text);
                return false;
            }
        });

        getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position,long id) {
                Intent courseDetailActivity = new Intent(getActivity(), CourseDetailActivity.class);
                courseDetailActivity.putExtra("course", courseList.get(position));
                startActivity(courseDetailActivity);
            }
        });
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        loadCourses();
        return inflater.inflate(R.layout.fragment_courses, container, false);
    }


    // JSON Loader helper methods
    public String AssetJSONFile () throws IOException {
        AssetManager manager = getActivity().getAssets();
        InputStream file = manager.open("sample-courses.json");
        byte[] formArray = new byte[file.available()];
        file.read(formArray);
        file.close();

        return new String(formArray);
    }

    private void loadCourses() {

        try
        {
            String jsonLocation = AssetJSONFile();
            JSONObject courses = new JSONObject(jsonLocation);
            JSONArray jarray = courses.getJSONArray("courses");

            for(int i=0;i<jarray.length();i++)
            {
                JSONObject jb =(JSONObject) jarray.get(i);

                String name = jb.getString("courseName");
                String instructor = jb.getString("instructor");
                String faculty = jb.getString("faculty");
                String code = jb.getString("courseCode");

                courseList.add(new Course(instructor, faculty, code, name));
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
