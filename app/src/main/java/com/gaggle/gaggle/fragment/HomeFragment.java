package com.gaggle.gaggle.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.support.v4.app.ListFragment;
import android.content.res.AssetManager;
import android.widget.ProgressBar;
import android.widget.Toast;


import com.gaggle.gaggle.CourseActivity;
import com.gaggle.gaggle.CourseDetailActivity;
import com.gaggle.gaggle.R;
import com.gaggle.gaggle.model.Course;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

import java.util.ArrayList;

public class HomeFragment extends ListFragment {

    private ArrayList<Course> courseList = new ArrayList<>();

    // dont' do this at home kids
    private int gcounter = 0;
    private ArrayList<String> gCourseCodeList = new ArrayList<>();

    ProgressBar progressBar;
    ArrayAdapter adapter;

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        progressBar = (ProgressBar) getView().findViewById(R.id.progress_bar);

        FirebaseFirestore db = FirebaseFirestore.getInstance();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        final DocumentReference userReference = db.collection("users").document(user.getUid());
        userReference.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        ArrayList<String> courseList = (ArrayList<String>) document.get("courses");
                        if (courseList == null){
                            courseList = new ArrayList<>();
                        }
                        queryCourses(courseList);
                    }
                }

            }
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        /*
        try
        {
            String jsonLocation = AssetJSONFile();
            JSONObject courses = new JSONObject(jsonLocation);
            JSONArray jarray = courses.getJSONArray("courses");

            for(int i=0;i<jarray.length();i++)
            {
                JSONObject jb =(JSONObject) jarray.get(i);

                String name = jb.getString("courseName");
                String instructor = jb.getString("instructor");
                String faculty = jb.getString("faculty");
                String code = jb.getString("courseCode");

                courseList.add(new Course(instructor, faculty, code, name));
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

*/

        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    public void queryCourses(final ArrayList<String> courseCodeList){
        //case where we have no courses
        if (courseCodeList.isEmpty()){
            createCourseList();
            return;
        }
        gCourseCodeList = courseCodeList;
        gcounter = 0;
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        //I don't know a better way of doing this, so here we are with some cool global variables
        System.out.println(courseCodeList);
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        for (int counter = 0; counter < courseCodeList.size(); counter++) {
            System.out.println(counter);
            db.collection("courses").whereEqualTo("courseCode", courseCodeList.get(counter)).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                @Override
                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                    if (task.isSuccessful()) {
                        for (DocumentSnapshot document : task.getResult()) {
                            //System.out.println(document.getId() + " => " + document.get("groupids"));
                            //ArrayList<String> groupids = (ArrayList<String>) document.get("groupids");
                            String courseName = (String) document.get("courseName");
                            String faculty = (String) document.get("faculty");
                            String courseCode = (String) document.get("courseCode");
                            courseList.add(new Course("No instructor", faculty, courseCode, courseName));
                        }
                        System.out.println("counter " + gcounter);
                        System.out.println(gCourseCodeList.size());
                        if (gcounter == gCourseCodeList.size()){
                            //we finished querying all of them, create the listview UI
                            createCourseList();
                        }
                    } else {
                        System.out.println("Error getting documents." + task.getException());
                    }
                }
            });
            gcounter ++;
        }
    }

    public void createCourseList (){
        final String [] listItems = new String[courseList.size()];
        for (int i =0;i<listItems.length;i++){
            listItems[i] = courseList.get(i).getCourseCode();
        }
        if (getActivity() == null){
            //if user rapid fire taps on the home page, sometimes activity is null & crashes
            return;
        }

        ArrayAdapter arrayAdapter = new ArrayAdapter(getActivity().getApplicationContext(), android.R.layout.simple_list_item_1, listItems);
        //setListAdapter(adapter);
        setListAdapter(arrayAdapter);
        //getListView().setAdapter(adapter);

        getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position,long id) {
                Intent intent = new Intent(getActivity(), CourseActivity.class);
                intent.putExtra("course", courseList.get(position));
                startActivity(intent);
            }
        });
        progressBar.setVisibility(View.GONE);
    }

    public String AssetJSONFile () throws IOException {
        AssetManager manager = getActivity().getAssets();
        InputStream file = manager.open("sample-added-courses.json");
        byte[] formArray = new byte[file.available()];
        file.read(formArray);
        file.close();

        return new String(formArray);
    }
}
