package com.gaggle.gaggle.fragment;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.text.InputType;
import android.util.Log;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;


import com.gaggle.gaggle.GroupDetailActivity;
import com.gaggle.gaggle.R;
import com.gaggle.gaggle.model.Group;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//google stuff
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import android.support.annotation.NonNull;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseUser;
import android.widget.ProgressBar;
import android.widget.Toast;

import co.chatsdk.core.dao.Thread;
import co.chatsdk.core.interfaces.ThreadType;
import co.chatsdk.core.session.NM;
import co.chatsdk.firebase.wrappers.UserWrapper;


public class GroupsFragment extends ListFragment {
    private ListView groupsPage;
    private ArrayList<Group> groupsList = new ArrayList<>();
    private ArrayList<String> groupIdsReference = new ArrayList<>();
    private String m_Text = "";
    ProgressBar progressBar;

    public static GroupsFragment newInstance() {
        GroupsFragment fragment = new GroupsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        FirebaseFirestore db = FirebaseFirestore.getInstance();

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        String email = user.getEmail();
        //get all the courses belonging to this email

        db.collection("users").whereEqualTo("email", email).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (DocumentSnapshot document : task.getResult()) {
                        System.out.println(document.getId() + " => " + document.get("groupids"));
                        ArrayList<String> groupids = (ArrayList<String>) document.get("groupids");
                        groupIdsReference = groupids;
                        queryGroups(groupids);
                    }
                } else {
                    System.out.println("Error getting documents." + task.getException());
                }
            }
        });

        FloatingActionButton addGroupButton = (FloatingActionButton) getView().findViewById(R.id.fab);
        progressBar = (ProgressBar) getView().findViewById(R.id.progress_bar);

        addGroupButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                setUpChooseDialog();
            }
        });
    }

    public void setUpChooseDialog (){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle("Create Group");

        builder.setMessage("Join a group or create one?");

        builder.setPositiveButton("Join", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                setUpJoinDialog();
            }
        });
        builder.setNegativeButton("Create", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                setUpAlertDialogInput();
            }
        });
        builder.show();
    }
    public void setUpJoinDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Input a group ID to join");

        final EditText input = new EditText(getActivity());

        input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_NORMAL);
        builder.setView(input);

        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                m_Text = input.getText().toString();
                joinNewGroup();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }
    public void setUpAlertDialogInput (){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Input name of group");

        final EditText input = new EditText(getActivity());

        input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_NORMAL);
        builder.setView(input);

        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                m_Text = input.getText().toString();
                addNewGroup();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    public void joinNewGroup(){
        //the grossest function you will ever see
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        if (groupIdsReference.contains(m_Text)){
            Toast.makeText(getActivity(), "You are already in that group",
                    Toast.LENGTH_SHORT).show();
            return;
        }
        final DocumentReference addedGroup = db.collection("groups").document(m_Text);

        //query for the group - return if it doesn't exist with a toast
        addedGroup.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        //Log.d(TAG, "DocumentSnapshot data: " + document.getData());
                        ArrayList<String> memberList = (ArrayList<String>) document.get("userids");
                        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                        String userId = user.getUid();
                        memberList.add(userId);

                        // add person to group chat
                        List<Thread> threads = NM.thread().getThreads(ThreadType.Private);
                        for (Thread thread : threads) {
                            if (m_Text.equals(thread.getEntityID())) {
                                UserWrapper wrapper = UserWrapper.initWithEntityId(userId);
                                wrapper.metaOn();
                                wrapper.onlineOn();
                                NM.thread().addUsersToThread(thread, wrapper.getModel()).subscribe(()->{return;});
                                thread.update();
                                break;
                            }
                        }

                        addedGroup.update("userids", memberList) .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                System.out.println("successfully updated user's groupids");
                                //update the user's groups
                                groupIdsReference.add(m_Text);
                                FirebaseFirestore db = FirebaseFirestore.getInstance();
                                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                                DocumentReference userReference = db.collection("users").document(user.getUid());

                                userReference.update("groupids", groupIdsReference) .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        System.out.println("successfully updated user's groupids");
                                        groupsList = new ArrayList<>();
                                        queryGroups(groupIdsReference);
                                    }
                                });
                            }
                        });
                    } else {
                        Toast.makeText(getActivity(), "No such group with that ID",
                                Toast.LENGTH_SHORT).show();
                        return;
                    }
                } else {
                    //Log.d(TAG, "get failed with ", task.getException());
                }
            }
        });
    }


    public void addNewGroup(){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        Map<String, Object> newGroup = new HashMap<>();
        ArrayList<String> emptyGroups = new ArrayList<String> ();
        emptyGroups.add(user.getUid());

        newGroup.put("name", m_Text);
        newGroup.put("userids", emptyGroups);

        DocumentReference addedGroup = db.collection("groups").document();

        addedGroup.set(newGroup)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        System.out.println("successfully added a new group");
                    }
        });

        //update the user's groups
        groupIdsReference.add(addedGroup.getId());

        DocumentReference userReference = db.collection("users").document(user.getUid());

        userReference.update("groupids", groupIdsReference) .addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                System.out.println("successfully updated user's groupids");
                groupsList = new ArrayList<>();
                queryGroups(groupIdsReference);
            }
        });
    }

    private void queryGroups (final ArrayList<String> groupids){
        System.out.println(groupids);

        FirebaseFirestore db = FirebaseFirestore.getInstance();

        //after database call to fetch our groupids, convert the ids into group objects
        //TODO: right now it fetches EVERY group and filters client side. There is probably a better way!
        db.collection("groups").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (DocumentSnapshot document : task.getResult()) {
                        String qGroupId = (String) document.getId();
                        if (groupids.contains(qGroupId)){
                            //System.out.println(document.getId() + " => " + document.get("name"));
                            String name = (String) document.get("name");
                            ArrayList<String> memberList = (ArrayList<String>) document.get("userids");
                            groupsList.add(new Group(document.getId(), name, memberList));
                        }
                    }
                    if (!groupsList.isEmpty()) {
                        populateGroupList();
                    }
                    progressBar.setVisibility(View.GONE);
                } else {
                    System.out.println("Error getting documents." + task.getException());
                }
            }
        });
    }

    public void populateGroupList (){
        //prevent crashes
        //use this list of groups to populate our list
        final String [] listItems = new String[groupsList.size()];
        for (int i =0;i<listItems.length;i++){
            listItems[i] = groupsList.get(i).getGroupName();
        }
        if (getActivity() == null){
            //if user rapid fire taps on the groups page, sometimes activity is null
            return;
        }
        ArrayAdapter arrayAdapter = new ArrayAdapter(getActivity().getApplicationContext(), android.R.layout.simple_list_item_1, listItems);

        setListAdapter(arrayAdapter);
        getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Group selectedGroup = groupsList.get(i);
                Intent groupDetailActivity = new Intent(getActivity().getApplicationContext(), GroupDetailActivity.class);

                groupDetailActivity.putExtra("title", selectedGroup.getGroupName());
                groupDetailActivity.putExtra("members", selectedGroup.getMemberList());
                groupDetailActivity.putExtra("groupId", selectedGroup.getGroupId());

                startActivity(groupDetailActivity);
            }
        });;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_groups, container, false);
        //initList();
        return view;
    }
    public String AssetJSONFile () throws IOException {
        AssetManager manager = getActivity().getAssets();
        InputStream file = manager.open("MOCK_GROUPS.json");
        byte[] formArray = new byte[file.available()];
        file.read(formArray);
        file.close();

        return new String(formArray);
    }

    private void initList(){
        //populates our json mock data from assets directory
        try {
            String jsonLocation = AssetJSONFile();
            JSONObject groups = new JSONObject(jsonLocation);
            JSONArray jarray = groups.getJSONArray("groups");


            for (int i = 0; i<jarray.length();i++){
                JSONObject jb = (JSONObject) jarray.get(i);

                JSONArray marray = jb.getJSONArray("members");

                final String [] memberList = new String[marray.length()];

                for (int j = 0; j<marray.length();j++) {
                    JSONObject gb = (JSONObject) marray.get(j);
                    String memberName = gb.getString("first_name") +  " " + gb.getString("last_name");
                    memberList[j] = memberName;
                }

                String name = jb.getString("group");

                //groupsList.add(new Group(name, memberList));
            }
        } catch (IOException e){
            e.printStackTrace();
        } catch (JSONException e){
            e.printStackTrace();
        }

    }
}
