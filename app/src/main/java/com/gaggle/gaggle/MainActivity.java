package com.gaggle.gaggle;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;


import com.gaggle.gaggle.fragment.CoursesFragment;
import com.gaggle.gaggle.fragment.GroupsFragment;
import com.gaggle.gaggle.fragment.HomeFragment;
import com.google.firebase.FirebaseOptions;

import co.chatsdk.core.session.ChatSDK;
import co.chatsdk.core.session.Configuration;
import co.chatsdk.core.session.NM;
import co.chatsdk.firebase.FirebaseModule;
import co.chatsdk.firebase.file_storage.FirebaseFileStorageModule;
import co.chatsdk.firebase.push.FirebasePushModule;
import co.chatsdk.ui.manager.UserInterfaceModule;
import co.chatsdk.ui.utils.AppBackgroundMonitor;
import io.reactivex.android.schedulers.AndroidSchedulers;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Context context = getApplicationContext();

        // Create a new configuration
        Configuration.Builder builder = new Configuration.Builder(context);

        // Perform any configuration steps (optional)
        builder.firebaseRootPath("prod");
        builder.locationMessagesEnabled(false);
        builder.imageMessagesEnabled(true);
        builder.firebaseCloudMessagingServerKey("AAAA5cU1teU:APA91bFrzaJMHen127p-hJDegU-21pDINNT_W7M_5hIMYMQsh5PGB5cUSrzPDpGJtGZCk7koRmUew5rgXPq9-rgq1Dh7C3SRJ-dybpHHmpAYW0e1PYifK8OlCGxuFRtVnlcj2c7UfFZ6JL6N9qro4B8e9b4_zoVddw");
//        builder.publicRoomCreationEnabled(true);

        // Initialize the Chat SDK
        ChatSDK.initialize(builder.build());
        UserInterfaceModule.activate(context);

        // Activate the Firebase module
        FirebaseModule.activate();

        // File storage is needed for profile image upload and image messages
        FirebaseFileStorageModule.activate();

        // for push notifications
        FirebasePushModule.activateForFirebase();

        NM.auth().authenticateWithCachedToken()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> {
                    AppBackgroundMonitor.shared().setEnabled(true);
                });

        setContentView(R.layout.activity_main);
        FirebaseOptions options = new FirebaseOptions.Builder()
                .setProjectId("gaggle-80d6c")
                .setApplicationId("com.gaggle.gaggle")
                .setApiKey("AIzaSyAasi_cSvqgpHpgwN5VcLS6Dyj6s68xoBk")
                .build();

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.actionbar);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener
                (new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        Fragment selectedFragment = null;
                        switch (item.getItemId()) {
                            case R.id.navigation_home:
                                selectedFragment = HomeFragment.newInstance();
                                break;
                            case R.id.navigation_courses:
                                selectedFragment = CoursesFragment.newInstance();
                                break;
                            case R.id.navigation_groups:
                                selectedFragment = GroupsFragment.newInstance();
                                break;
                        }
                        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                        transaction.replace(R.id.fragment_container, selectedFragment);
                        transaction.commit();
                        return true;
                    }
                });
        //Manually displaying the first fragment - one time only
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, HomeFragment.newInstance());
        transaction.commit();

        //Used to select an item programmatically
        //bottomNavigationView.getMenu().getItem(2).setChecked(true);
    }
}
