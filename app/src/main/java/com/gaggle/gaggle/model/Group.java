package com.gaggle.gaggle.model;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Group implements Serializable{
    private String groupId;
    private String groupName;

    //contains the firebase IDs for members of the group
    ArrayList<String> memberList;

    public Group (String groupId,String groupName, ArrayList<String> memberList){
        this.groupId = groupId;
        this.groupName = groupName;
        this.memberList = memberList;
    }

    public String getGroupId(){
        return groupId;
    }

    public String getGroupName(){
        return groupName;
    }

    public void setGroupName(String groupName){
        this.groupName = groupName;
    }

    public ArrayList<String> getMemberList (){
        return memberList;
    }

    public void addMember (String member){
        memberList.add(member);
    }
    public void setMemberList (ArrayList<String> memberList){
        this.memberList = memberList;
    }

    public void updateGroup (){
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        Map<String, Object> newGroup = new HashMap<>();
        ArrayList<String> emptyGroups = new ArrayList<String> ();

        newGroup.put("name", this.groupName);
        newGroup.put("userids", this.memberList);

        db.collection("groups").document(this.groupId)
                .set(newGroup)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        System.out.println("successfully updated a group");
                    }
        });
    }
}
