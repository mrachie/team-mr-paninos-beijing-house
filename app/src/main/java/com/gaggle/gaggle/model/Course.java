package com.gaggle.gaggle.model;

import java.io.Serializable;

public class Course implements Serializable {

    private String instructor;
    private String faculty;
    private String courseCode;
    private String courseName;

    public Course (String instructor,
                   String faculty,
                   String courseCode,
                   String courseName) {
        this.instructor = instructor;
        this.faculty = faculty;
        this.courseCode = courseCode;
        this.courseName = courseName;
    }

    public String getInstructor() {
        return instructor;
    }

    public void setInstructor(String instructor) {
        this.instructor = instructor;
    }

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    public String getCourseCode() {
        return courseCode;
    }

    public void setCourseCode(String courseCode) {
        this.courseCode = courseCode;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }
}
