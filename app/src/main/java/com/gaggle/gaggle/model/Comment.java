package com.gaggle.gaggle.model;

import java.io.Serializable;

public class Comment implements Serializable {

    private String postID;
    private String username;
    private String timestamp;
    private String message;

    public Comment(String postID, String username, String commentTime,
                   String message) {
        this.postID = postID;
        this.username = username;
        this.timestamp = commentTime;
        this.message = message;
    }

    public String getPostID() {
        return postID;
    }

    public void setPostID(String postID) {
        this.postID = postID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
