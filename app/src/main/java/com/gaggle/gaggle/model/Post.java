package com.gaggle.gaggle.model;

import java.io.Serializable;

public class Post implements Serializable {

    private String postID;
    private String username;
    private String messageTime;
    private String message;
    private long messageCount;

    public Post() {} // no arg constructor

    public Post(String postID, String username, String messageTime,
                String message, long messageCount) {
        this.postID = postID;
        this.username = username;
        this.messageTime = messageTime;
        this.message = message;
        this.messageCount = messageCount;
    }

    public String getPostID() {
        return postID;
    }

    public void setPostID(String id) {
        this.postID = postID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMessageTime() {
        return messageTime;
    }

    public void setMessageTime(String messageTime) {
        this.messageTime = messageTime;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getMessageCount() {
        return messageCount;
    }

    public void setMessageCount(long messageCount) {
        this.messageCount = messageCount;
    }
}
