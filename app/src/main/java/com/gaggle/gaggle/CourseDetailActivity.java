package com.gaggle.gaggle;

import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.gaggle.gaggle.model.Course;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;

public class CourseDetailActivity extends AppCompatActivity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course_detail);

        //back button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final Course selectedCourse = (Course)this.getIntent().getExtras().getSerializable("course");

        setTitle(R.string.title_course_detail);

        //set up add button
        FloatingActionButton addGroupButton = (FloatingActionButton) this.findViewById(R.id.fab);

        addGroupButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //add this course code to the user courses table
                FirebaseFirestore db = FirebaseFirestore.getInstance();
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                final DocumentReference userReference = db.collection("users").document(user.getUid());
                userReference.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()) {
                            DocumentSnapshot document = task.getResult();
                            if (document.exists()) {
                                ArrayList<String> courseList = (ArrayList<String>) document.get("courses");
                                if (courseList == null){
                                    courseList = new ArrayList<>();
                                }
                                if (!courseList.contains(selectedCourse.getCourseCode())) {
                                    courseList.add(selectedCourse.getCourseCode());
                                    userReference.update("courses", courseList) .addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {
                                            System.out.println("successfully updated user's courses");
                                            Toast.makeText(CourseDetailActivity.this, "Added " + selectedCourse.getCourseCode() + " to your courses",
                                                    Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }else{
                                    Toast.makeText(CourseDetailActivity.this, "You already have this course added",
                                            Toast.LENGTH_SHORT).show();
                                }
                            }
                        }

                    }
                });
            }
        });

        TextView courseDetails = findViewById(R.id.course_details);
        String details = selectedCourse.getCourseCode() + '\n'
                + selectedCourse.getCourseName() + '\n'
                + "Instructor: " + selectedCourse.getInstructor() + '\n'
                + "Faculty: " + selectedCourse.getFaculty() + "\n\n"
                + "Description: Lorem ipsum dolor sit amet, ad viris malorum insolens his, nam postea"
                + " delenit scaevola ei, et movet deseruisse mea. An mei sint vituperata, ei nonumy "
                + "vocent eos, lorem tation persequeris mei at. Ut prompta delectus gloriatur vix. "
                + "Virtute copiosae pro in, usu ei mollis equidem. Ut meliore cotidieque usu, sit cu"
                + " omnis facilis. Vis justo dolor epicurei id. At facete interesset mei, vel solum "
                + "graece graeco et, ei ludus eruditi gubergren pro.";
        courseDetails.setText(details);
    }

    public boolean onOptionsItemSelected(MenuItem item){
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
