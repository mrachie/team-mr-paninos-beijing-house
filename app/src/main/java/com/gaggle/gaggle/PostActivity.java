package com.gaggle.gaggle;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.gaggle.gaggle.adapter.CommentListViewAdapter;
import com.gaggle.gaggle.model.Comment;
import com.gaggle.gaggle.model.Post;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PostActivity extends AppCompatActivity {

    private ProgressBar progressBar;
    private FirebaseFirestore db;
    private List<Object> comments = new ArrayList<>();
    private Post post;
    private EditText commentEditTextView;
    private Button sendButton;
    private CommentListViewAdapter commentListViewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);

        post = (Post) getIntent().getSerializableExtra("post");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.title_comments);
        setSupportActionBar(toolbar);
        progressBar = (ProgressBar) this.findViewById(R.id.progress_bar);
        commentEditTextView = (EditText) this.findViewById(R.id.commentEditText);
        sendButton = (Button) this.findViewById(R.id.sendButton);

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message = commentEditTextView.getText().toString();
                if(!message.equals("")){
                    Map<String, Object> comment = new HashMap<>();
                    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                    comment.put("message", message);
                    comment.put("username", user.getDisplayName());
                    comment.put("timestamp", String.valueOf(Instant.now().toEpochMilli()));
                    comment.put("postID", post.getPostID());
                    db.collection("comments").add(comment);
                    db.collection("posts")
                            .document(post.getPostID())
                            .update("commentCount", post.getMessageCount() + 1);
                    post.setMessageCount(post.getMessageCount() + 1);
                    comments.set(0, post);
                    query();

                    commentListViewAdapter.notifyDataSetChanged();

                    commentEditTextView.setText("");
                }
            }
        });

        // add back arrow to toolbar
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        query();
    }

    private void query() {
        //Initialize Firestore db
        db = FirebaseFirestore.getInstance();
        db.collection("comments")
                .orderBy("timestamp", Query.Direction.ASCENDING)
                .whereEqualTo("postID", post.getPostID())
                .get()
                .addOnCompleteListener(getOnCompleteListener());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {

            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }

    public OnCompleteListener<QuerySnapshot> getOnCompleteListener() {
        return new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {

                    comments.clear();

                    for (QueryDocumentSnapshot document : task.getResult()) {

                        Map<String, Object> postMap = document.getData();
                        Comment comment = new Comment((String) postMap.get("postID"),
                                (String) postMap.get("username"),
                                (String) postMap.get("timestamp"),
                                (String) postMap.get("message"));
                        comments.add(comment);
                    }

                    comments.add(0,post);

                    ListView listView = findViewById(R.id.listView);
                    commentListViewAdapter = new CommentListViewAdapter(PostActivity.this, comments);
                    listView.setAdapter(commentListViewAdapter);
                } else {
                    System.out.println("Error getting documents." + task.getException());
                }

                progressBar.setVisibility(View.GONE);
            }
        };
    }
}
