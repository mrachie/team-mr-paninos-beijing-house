package com.gaggle.gaggle.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.gaggle.gaggle.PostViewHolder;
import com.gaggle.gaggle.R;
import com.gaggle.gaggle.model.Post;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.TextStyle;
import java.util.List;
import java.util.Locale;

public class ListViewAdapter extends BaseAdapter {

    private Context context;
    private List<Post> listPosts;

    public ListViewAdapter(Context context, List<Post> listTwitterRows) {
        this.context = context;
        this.listPosts = listTwitterRows;
    }

    @Override
    public int getCount() {
        return listPosts.size();
    }

    @Override
    public Object getItem(int position) {
        return listPosts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        PostViewHolder viewHolder;

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.course_list_layout, null);
            viewHolder = new PostViewHolder();
            viewHolder.imageViewProfile = convertView.findViewById(R.id.imageViewProfile);
            viewHolder.textViewUsername = convertView.findViewById(R.id.textViewUsername);
            viewHolder.textViewMessageTime = convertView.findViewById(R.id.textViewMessageTime);
            viewHolder.textViewMessageText = convertView.findViewById(R.id.textViewMessageText);
            viewHolder.imageViewProfile.setImageResource(R.drawable.ic_baseline_person_outline_24px);

            viewHolder.imageViewChats = convertView.findViewById(R.id.imageViewChats);
            viewHolder.textViewChats = convertView.findViewById(R.id.textViewChats);
            viewHolder.imageViewPrivateMessage = convertView.findViewById(R.id.imageViewPrivateMessage);
            viewHolder.textViewPrivateMessage = convertView.findViewById(R.id.textViewPrivateMessage);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (PostViewHolder) convertView.getTag();
        }

        Post post = listPosts.get(position);

        viewHolder.textViewUsername.setText(post.getUsername());

        Instant instant = Instant.ofEpochMilli(Long.valueOf(post.getMessageTime()));
        LocalDateTime ldt = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
        String date = String.format("%s %d", ldt.getMonth().getDisplayName(TextStyle.SHORT, Locale.CANADA), ldt.getDayOfMonth());
        viewHolder.textViewMessageTime.setText(date);
        viewHolder.textViewMessageText.setText(post.getMessage());

        viewHolder.textViewChats.setText(String.format("%d Comments", post.getMessageCount()));
        viewHolder.textViewPrivateMessage.setText("Message");

        return convertView;
    }
}
