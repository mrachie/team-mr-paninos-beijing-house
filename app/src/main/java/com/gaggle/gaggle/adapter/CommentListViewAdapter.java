package com.gaggle.gaggle.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.gaggle.gaggle.PostViewHolder;
import com.gaggle.gaggle.R;
import com.gaggle.gaggle.model.Comment;
import com.gaggle.gaggle.model.Post;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.TextStyle;
import java.util.List;
import java.util.Locale;

public class CommentListViewAdapter extends BaseAdapter {

    private Context context;
    private List<Object> comments;

    public CommentListViewAdapter(Context context, List<Object> comments) {
        this.context = context;
        this.comments = comments;
    }

    @Override
    public int getCount() {
        return comments.size();
    }

    @Override
    public Object getItem(int position) {
        return comments.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        return (position == 0) ? 0 : 1; // 0 post type, 1 comment type
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Object row = comments.get(position);

        if (convertView == null) {

            int type = getItemViewType(position);

            if(type == 0) {
                convertView = LayoutInflater.from(context).inflate(R.layout.course_list_layout, null);
                PostViewHolder viewHolder = new PostViewHolder();

                viewHolder.imageViewProfile = convertView.findViewById(R.id.imageViewProfile);
                viewHolder.textViewUsername = convertView.findViewById(R.id.textViewUsername);
                viewHolder.textViewMessageTime = convertView.findViewById(R.id.textViewMessageTime);
                viewHolder.textViewMessageText = convertView.findViewById(R.id.textViewMessageText);
                viewHolder.imageViewChats = convertView.findViewById(R.id.imageViewChats);
                viewHolder.textViewChats = convertView.findViewById(R.id.textViewChats);
                viewHolder.imageViewPrivateMessage = convertView.findViewById(R.id.imageViewPrivateMessage);
                viewHolder.textViewPrivateMessage = convertView.findViewById(R.id.textViewPrivateMessage);
                convertView.setTag(viewHolder);

                viewHolder.textViewUsername.setText(((Post) row).getUsername());

                Instant instant = Instant.ofEpochMilli(Long.valueOf(((Post) row).getMessageTime()));
                LocalDateTime ldt = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
                String date = String.format("%s %d", ldt.getMonth().getDisplayName(TextStyle.SHORT, Locale.CANADA), ldt.getDayOfMonth());
                viewHolder.textViewMessageTime.setText(date);
                viewHolder.textViewMessageText.setText(((Post) row).getMessage());

                viewHolder.textViewChats.setText(String.format("%d Comments", ((Post) row).getMessageCount()));
                viewHolder.textViewPrivateMessage.setText("Message");

            } else if(type == 1) {
                convertView = LayoutInflater.from(context).inflate(R.layout.comment_list_layout, null);
                CommentViewHolder viewHolder = new CommentViewHolder();

                viewHolder.imageViewProfile = convertView.findViewById(R.id.imageViewProfile);
                viewHolder.textViewUsername = convertView.findViewById(R.id.textViewUsername);
                viewHolder.textViewCommentTime = convertView.findViewById(R.id.textViewCommentTime);
                viewHolder.textViewCommentText = convertView.findViewById(R.id.textViewComment);
                convertView.setTag(viewHolder);

                viewHolder.textViewUsername.setText(((Comment) row).getUsername());

                Instant instant = Instant.ofEpochMilli(Long.valueOf(((Comment) row).getTimestamp()));
                LocalDateTime ldt = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
                String date = String.format("%s %d", ldt.getMonth().getDisplayName(TextStyle.SHORT, Locale.CANADA), ldt.getDayOfMonth());
                viewHolder.textViewCommentTime.setText(date);
                viewHolder.textViewCommentText.setText(((Comment) row).getMessage());
            }
        }

        return convertView;
    }

    class CommentViewHolder {
        ImageView imageViewProfile;
        TextView textViewUsername;
        TextView textViewCommentTime;
        TextView textViewCommentText;
    }
}
