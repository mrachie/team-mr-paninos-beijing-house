package com.gaggle.gaggle;

import android.Manifest;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.CalendarContract;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.view.View;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.TimeZone;

import android.app.TimePickerDialog;
import android.widget.TimePicker;


import com.gaggle.gaggle.model.Group;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import android.support.annotation.NonNull;
import android.widget.Toast;

import java.util.List;
import java.util.ArrayList;

import co.chatsdk.core.dao.User;
import co.chatsdk.firebase.wrappers.UserWrapper;
import co.chatsdk.ui.manager.InterfaceManager;
import co.chatsdk.core.session.NM;
import co.chatsdk.core.interfaces.ThreadType;
import io.reactivex.android.schedulers.AndroidSchedulers;

import co.chatsdk.core.dao.Thread;

public class GroupDetailActivity extends AppCompatActivity implements View.OnClickListener {
    ArrayList<String> memberNameList = new ArrayList<>();
    ArrayList<ArrayList<String>> memberInfoList = new ArrayList<>();
    private String m_Text = "";
    private Group currentGroup;
    final Calendar cal = Calendar.getInstance();
    Button btnTimePicker;
    TextView txtTime;
    private int mYear, mMonth, mDay, mHour, mMinute;
    final int MY_CAL_WRITE_REQ = 0;
    String title = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_detail);

        final Context context = getApplicationContext();
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        //back button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final String groupId = this.getIntent().getExtras().getString("groupId");
        title = this.getIntent().getExtras().getString("title");
        final ArrayList<String> memberList =  this.getIntent().getExtras().getStringArrayList("members");

        currentGroup = new Group(groupId, title, memberList);

        String memberCount = Integer.toString(memberList.size());

        setTitle(title);

        TextView membersTitle = (TextView) findViewById(R.id.members_title);
        membersTitle.setText(getString(R.string.title_members) + " (" + memberCount + ")");
        TextView chooseDateLabel = findViewById(R.id.choose_date_label);
        chooseDateLabel.setText(getString(R.string.choose_date_label));

        final Button addMembersButton = findViewById(R.id.add_members_button);
        final Button copyGroupIdButton = findViewById(R.id.copy_group_id_button);
        final ImageButton chatButton = findViewById(R.id.social_media_button);

        final ImageButton scheduleButton = findViewById(R.id.date_button);
        CalendarView calendarView = findViewById(R.id.simpleCalendarView);
        final Calendar cal = Calendar.getInstance();

        chatButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                List<Thread> threads = NM.thread().getThreads(ThreadType.Private);
                String id = currentGroup.getGroupId();
                for (Thread thread : threads) {
                    if (id.equals(thread.getEntityID())) {
                        InterfaceManager.shared().a.startMainActivity(context);
                        InterfaceManager.shared().a.startChatActivityForID(context, thread.getEntityID());
                        return;
                    }
                }
                //Generate group user list
                List<User> groupUser = new ArrayList<User>();
                for (ArrayList<String> member : memberInfoList) {
                    String userID = member.get(2);
                    UserWrapper wrapper = UserWrapper.initWithEntityId(userID);
                    wrapper.metaOn();
                    wrapper.onlineOn();
                    groupUser.add(wrapper.getModel());
                }

                //Create new private thread
                NM.thread().createThread(title, groupUser, ThreadType.PrivateGroup, id).observeOn(AndroidSchedulers.mainThread())
                        .subscribe(thread -> {
                            NM.thread().addUsersToThread(thread, groupUser).subscribe(()->{return;});
                            thread.update();
                            InterfaceManager.shared().a.startMainActivity(context);
                            InterfaceManager.shared().a.startChatActivityForID(context, thread.getEntityID());
                        });
            }
        });

        addMembersButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                setUpAlertDialogInput();
            }
        });

        copyGroupIdButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("groupid", groupId);
                clipboard.setPrimaryClip(clip);
                Toast.makeText(GroupDetailActivity.this, R.string.copied_group_id_message,
                        Toast.LENGTH_SHORT).show();
            }
        });

        db.collection("users").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (DocumentSnapshot document : task.getResult()) {
                        String qGroupId = (String) document.getId();
                        if (memberList.contains(qGroupId)){
                            //System.out.println(document.getId() + " => " + document.get("name"));
                            String name = (String) document.get("name");
                            String email = (String) document.get("email");
                            String userID = (String) document.getId();
                            String[] infoList = new String[]{name, email, userID};
                            memberNameList.add(name);
                            memberInfoList.add(new ArrayList(Arrays.asList(infoList)));
                        }
                    }
                    populateMemberList();
                } else {
                    System.out.println("Error getting documents." + task.getException());
                }
            }
        });

        btnTimePicker=(Button)findViewById(R.id.btn_time);
        btnTimePicker.setOnClickListener(this);
        txtTime=(TextView) findViewById(R.id.in_time);
        SimpleDateFormat date = new SimpleDateFormat("HH:mm");
        txtTime.setText(date.format(cal.getTime()));

        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange (CalendarView view, int year, int month, int dayOfMonth) {
                mYear = year;
                mMonth = month;
                mDay = dayOfMonth;
            }
        });

        scheduleButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                createEvent(title, memberInfoList);
            }
        });
    }

    public void alertEventScheduled() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.add_event_message);

        builder.setPositiveButton(R.string.ok_button, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    public void setUpAlertDialogInput (){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.input_new_email_prompt);

        final EditText input = new EditText(this);

        input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        builder.setView(input);

        // Set up the buttons
        builder.setPositiveButton(R.string.ok_button, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                m_Text = input.getText().toString();
                addGroupMember();
            }
        });
        builder.setNegativeButton(R.string.cancel_button, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    public void addGroupMember (){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        //with the input e-mail, find user's document ID and add it to memberlist

        db.collection("users").whereEqualTo("email", m_Text).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                Boolean foundUser = false;
                if (task.isSuccessful()) {
                    for (DocumentSnapshot document : task.getResult()) {
                        foundUser = true;
                        String currentGroupId = currentGroup.getGroupId();
                        String userId = (String) document.getId();
                        String name = (String) document.get("name");
                        String email = (String) document.get("email");
                        ArrayList<String> groupids = (ArrayList<String>) document.get("groupids");
                        groupids.add(currentGroupId);
                        String[] infoList = new String[]{name, email};

                        memberNameList.add(name);
                        memberInfoList.add(new ArrayList(Arrays.asList(infoList)));
                        currentGroup.addMember(userId);
                        currentGroup.updateGroup();
                        //TODO: update added user's groupids
                        updateAddedUser(userId, groupids);
                        populateMemberList();

                        // add person to group chat
                        List<Thread> threads = NM.thread().getThreads(ThreadType.Private);
                        for (Thread thread : threads) {
                            if (currentGroupId.equals(thread.getEntityID())) {
                                UserWrapper wrapper = UserWrapper.initWithEntityId(userId);
                                wrapper.metaOn();
                                wrapper.onlineOn();
                                NM.thread().addUsersToThread(thread, wrapper.getModel()).subscribe(()->{return;});
                                thread.update();
                                break;
                            }
                        }
                    }
                } else {
                    System.out.println("Error getting documents." + task.getException());
                }

                if (!foundUser){
                    Toast.makeText(GroupDetailActivity.this, "No user with that e-mail",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void updateAddedUser(String userId, ArrayList<String> groupids){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference userReference = db.collection("users").document(userId);

        userReference.update("groupids", groupids) .addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                System.out.println("successfully updated user's groupids");
            }
        });
    }

    public void populateMemberList (){
        ListView groupMembers = (ListView) findViewById(R.id.group_detail_members);
        final ArrayAdapter arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, memberNameList);
        groupMembers.setAdapter(arrayAdapter);
        setListViewHeightBasedOnChildren(groupMembers);
    }

    @Override
    public void onClick(View v) {
        if (v == btnTimePicker) {

            // Get Current Time
            final Calendar c = Calendar.getInstance();
            mHour = c.get(Calendar.HOUR_OF_DAY);
            mMinute = c.get(Calendar.MINUTE);

            // Launch Time Picker Dialog
            TimePickerDialog timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {

                @Override
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    String colon = ":";
                    if (minute < 10) {
                        colon += "0";
                    }
                    
                    txtTime.setText(hourOfDay + colon + minute);
                    cal.set(Calendar.HOUR_OF_DAY, hourOfDay);
                    cal.set(Calendar.MINUTE, minute);
                }}, mHour, mMinute, false);
            timePickerDialog.show();
        }
    }

    public void addAttendee(String name, String email, String eventId) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_CALENDAR}, MY_CAL_WRITE_REQ);
        }
        ContentResolver cr = getContentResolver();

        ContentValues values = new ContentValues();
        values.put(CalendarContract.Attendees.ATTENDEE_NAME, name);
        values.put(CalendarContract.Attendees.ATTENDEE_TYPE, CalendarContract.Attendees.TYPE_REQUIRED);
        values.put(CalendarContract.Attendees.ATTENDEE_EMAIL, email);
        values.put(CalendarContract.Attendees.ATTENDEE_STATUS, CalendarContract.Attendees.ATTENDEE_STATUS_INVITED);
        values.put(CalendarContract.Attendees.ATTENDEE_RELATIONSHIP, CalendarContract.Attendees.RELATIONSHIP_SPEAKER);
        values.put(CalendarContract.Attendees.EVENT_ID, eventId);

        cr.insert(CalendarContract.Attendees.CONTENT_URI, values);
    }

    public void createEvent(String title, ArrayList<ArrayList<String>> memberInfoList) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_CALENDAR}, MY_CAL_WRITE_REQ);
        } else {
            ContentResolver cr = getContentResolver();
            ContentValues eventValues = new ContentValues();

            cal.set(Calendar.YEAR, mYear);
            cal.set(Calendar.MONTH, mMonth);
            cal.set(Calendar.DAY_OF_MONTH, mDay);

            eventValues.put(CalendarContract.Events.CALENDAR_ID, 3); // for Android version M or higher
            eventValues.put(CalendarContract.Events.TITLE, title + " meeting");
            eventValues.put(CalendarContract.Events.EVENT_TIMEZONE, TimeZone.getDefault().getID());
            eventValues.put(CalendarContract.Events.DTSTART, cal.getTimeInMillis());
            eventValues.put(CalendarContract.Events.DTEND, cal.getTimeInMillis()+60*60*1000);

            try {
                Uri eventUri = cr.insert(CalendarContract.Events.CONTENT_URI, eventValues);

                //to get the last inserted event id
                long eventId = Long.parseLong(eventUri.getLastPathSegment());

                for (ArrayList<String> member : memberInfoList) {
                    String name = member.get(0);
                    String email = member.get(1);
                    addAttendee(name, email, Long.toString(eventId));
                }
                alertEventScheduled();
            } catch (Exception e) {
                Toast.makeText(GroupDetailActivity.this, "Please install the Google Calendar app and log in to access this feature.",
                        Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[]
            grantResults) {
        switch (requestCode) {
            case MY_CAL_WRITE_REQ:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    createEvent(title, memberInfoList);
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    public boolean onOptionsItemSelected(MenuItem item){
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            return;
        }
        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }
}
