package com.gaggle.gaggle;

import android.widget.ImageView;
import android.widget.TextView;

public class PostViewHolder {
    public ImageView imageViewProfile;
    public TextView textViewUsername;
    public TextView textViewMessageTime;
    public TextView textViewMessageText;

    public ImageView imageViewChats;
    public TextView textViewChats;
    public ImageView imageViewPrivateMessage;
    public TextView textViewPrivateMessage;
}